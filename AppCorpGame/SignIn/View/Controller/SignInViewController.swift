//
//  SignInViewController.swift
//  AppCorpGame
//
//  Created by Islam Abotaleb on 8/5/19.
//  Copyright © 2019 Islam Abotaleb. All rights reserved.
//

import UIKit
class SignInViewController: BaseViewController {
    @IBOutlet weak var signinbtn: UIButton!
    @IBOutlet weak var passwordtxt: UITextField!
    @IBOutlet weak var emailtxt: UITextField!
//
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
      
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0.6274509804, blue: 0.6352941176, alpha: 1)
        self.navigationController?.navigationBar.topItem!.title = "SignIn"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
      
        
        AppUtils.shared.addBorderToBtn(button: signinbtn)
        AppUtils.shared.addBorderToTextField(button: emailtxt)
        AppUtils.shared.addBorderToTextField(button: passwordtxt)
    }
    
    @IBAction func signInAction(_ sender: Any) {
//        if (passwordtxt.text?.isEmpty)! && (emailtxt.text?.isEmpty)! {
//            print("Must be Insert All Data")
//        } else if  (passwordtxt.text?.isEmpty)! {
//            print("Must be insert password")
//        }  else if (emailtxt.text?.isEmpty)! {
//            print("Must be insert email text")
//        }else {
            let mainvc = SelectViewController.init(nibName: "SelectViewController", bundle: nil)
            let navgationController = UINavigationController(rootViewController: mainvc)
            self.present(navgationController, animated: true, completion: nil)
//        }

        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
        let isEmailAddressValid = isValidEmailAddress(emailAddressString: self.emailtxt.text!)
        if isEmailAddressValid {
            print("EmailisValid")
        } else {
            print("notvalid")
        }

    }
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
