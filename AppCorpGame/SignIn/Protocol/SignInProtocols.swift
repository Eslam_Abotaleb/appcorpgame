//
//  SignInProtocols.swift
//  AppCorpGame
//
//  Created by Islam Abotaleb on 8/6/19.
//  Copyright © 2019 Islam Abotaleb. All rights reserved.
//

import Foundation
protocol SignInViewProtocol {
    var presenter: SigninPresenterProtocol! {get set}
}
protocol SigninPresenterProtocol: class {
    //viewDidload
    func viewDidLoad()
    //viewDidapear
    func ViewWillAppear()
}
protocol SignInInteractorInputProtocol {
    
}
protocol SignInInteractorOutputProtocol {
    
}
protocol SignInRouterProtocol {
    
}
