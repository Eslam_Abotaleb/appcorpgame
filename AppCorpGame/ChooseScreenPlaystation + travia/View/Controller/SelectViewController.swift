//
//  SelectViewController.swift
//  AppCorpGame
//
//  Created by Islam Abotaleb on 8/5/19.
//  Copyright © 2019 Islam Abotaleb. All rights reserved.
//

import UIKit

class SelectViewController: BaseViewController {

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0.6274509804, blue: 0.6352941176, alpha: 1)
        
        self.navigationController?.navigationBar.topItem!.title = "AppCorpGame"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: makeBackButton())
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func makeBackButton() -> UIButton {
//        let backButtonImage = UIImage(named: "backbutton")?.withRenderingMode(.alwaysTemplate)
        let backButton = UIButton(type: .custom)
//        backButton.setImage(backButtonImage, for: .normal)
        backButton.tintColor = .white
        backButton.setTitle("Back", for: .normal)
        backButton.setTitleColor(.white, for: .normal)
        backButton.addTarget(self, action: #selector(self.backButtonPressed), for: .touchUpInside)
        return backButton
    }
    
    @objc func backButtonPressed() {
       dismiss(animated: true, completion: nil)
        print("backbuttonispressed")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
