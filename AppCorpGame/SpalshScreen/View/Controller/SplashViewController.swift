//
//  SplashViewController.swift
//  AppCorpGame
//
//  Created by Islam Abotaleb on 8/5/19.
//  Copyright © 2019 Islam Abotaleb. All rights reserved.
//

import UIKit

class SplashViewController: BaseViewController {

//    override func viewWillAppear(_ animated: Bool) {
//            print("dd")
//        
//    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        afterFinishSplashScreen {
            self.loadSignInView()
        }
    }
    
    func afterFinishSplashScreen(finish: @escaping ()-> Void) {
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
