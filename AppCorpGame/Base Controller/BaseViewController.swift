//
//  BaseViewController.swift
//  AppCorpGame
//
//  Created by Islam Abotaleb on 8/5/19.
//  Copyright © 2019 Islam Abotaleb. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        view.addGestureRecognizer(edgePan)
    }
    
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            print("Screen")
        }
    }
    
    func loadSignInView() {
        appDelegate.window?.rootViewController = nil
        let viewController = SignInViewController.init(nibName: "SignIn", bundle: nil)
        let viewNav = UINavigationController(rootViewController: viewController)
        appDelegate.window?.rootViewController = viewNav
        appDelegate.window?.makeKeyAndVisible()
    }
}

